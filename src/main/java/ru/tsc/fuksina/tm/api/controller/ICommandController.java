package ru.tsc.fuksina.tm.api.controller;

public interface ICommandController {

    void showVersion();

    void showAbout();

    void showSystemInfo();

    void showHelp();

    void showCommands();

    void showArguments();

}
